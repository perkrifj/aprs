package gps;

/**
 * This class calculates the distance <i> d </i> between two <latitude,longitude> pairs
 * 
 * @see <url> http://en.wikipedia.org/wiki/Haversine_formula </url>
 * 
 *  Based on: http://www.movable-type.co.uk/scripts/latlong.html
 */
public class Haversine {
	
	public final static int earthRadius = 6371; //km

//	public static double haversineFunction(double dLat, double dLon, double lat1, double lat2){
//		double a = Math.sin(dLat/2) * Math.sin(dLat/2) + Math.sin(dLon/2) * Math.sin(dLon/2) * Math.cos(lat1) * Math.cos(lat2);
//		double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
//		return earthRadius*c;
//	}
//	
	/**
	 * 
	 * Input must be in degrees, i.e. 63.0132, 10.90213
	 * 
	 */
	public static double getDistance(double lat1, double lon1, double lat2, double lon2){
		
		double dLat = Math.toRadians(lat2-lat1);
		double dLon = Math.toRadians(lon2-lon1);
		
		double sindLat = Math.sin(dLat / 2);
	    double sindLng = Math.sin(dLon / 2);
	    
	    double a = Math.pow(sindLat, 2) + Math.pow(sindLng, 2)
	            * Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2));
	    double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
	    double dist = earthRadius * c;
		
		return dist;
	}
	
	public static void main(String[] args) {
		double lat1 = 63.424801;
		double lon1 = 10.429931;
		double lat2 = 63.424755;
		double lon2 = 10.429833;
		System.out.println(getDistance(lat1, lon1, lat2, lon2));
	}
}
