package aprs;

import java.util.ArrayList;
public class Aprs {

	private String command;
	private String result;
	private String what;
	private int found;
	private ArrayList<Attributes> entries;
	
	
	public String getCommand() {
		return command;
	}
	public void setCommand(String command) {
		this.command = command;
	}
	public String getResult() {
		return result;
	}
	public void setResult(String result) {
		this.result = result;
	}
	public String getWhat() {
		return what;
	}
	public void setWhat(String what) {
		this.what = what;
	}
	public int getFound() {
		return found;
	}
	public void setFound(int found) {
		this.found = found;
	}
	public ArrayList<Attributes> getEntries() {
		return entries;
	}
	public void setEntries(ArrayList<Attributes> entries) {
		this.entries = entries;
	}
}
