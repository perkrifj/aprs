package aprs;

import gps.Haversine;

import java.io.IOException;
import java.util.Date;
import java.util.Scanner;

public class Track {
	
	public final static double latSamfundet = 63.422106;
	public final static double lonSamfundet = 10.396017;
	public double distance;
	public long time;
	private Aprs aprsRecord;
	
	public double getDistance() {
		return distance;
	}

	public void setDistance(double distance) {
		this.distance = distance;
	}

	public long getTime() {
		return time;
	}

	public void setTime(long time) {
		this.time = time;
	}
	
	public void setAprsRecord(Aprs aprs){
		this.aprsRecord = aprs;
	}
	
	public Aprs getAprsRecord(){
		return aprsRecord;
	}

	public void setParameters(String call){
		try {
			this.setAprsRecord(GsonParse.gsonToObject(GsonParse.aprsfiRetrieveJSONString(call)));
		} catch (IOException e) {
			e.printStackTrace();
		}
		if(aprsRecord.getResult().equals("ok")){
			setTime(aprsRecord.getEntries().get(0).getTime());
			setDistance(Haversine.getDistance(aprsRecord.getEntries().get(0).getLat(), aprsRecord.getEntries().get(0).getLon(), latSamfundet, lonSamfundet));
		}
		else System.out.println("We failed..");
	}
	
	public static void main(String[] args) {
		Track t = new Track();
		System.out.println("Please type in the callsign you want to track");
		Scanner scan = new Scanner(System.in);
		String callsign = scan.next();
		scan.close();
		t.setParameters(callsign);
		System.out.println("Tracking "+callsign);
		System.out.println(callsign + " is " + t.getDistance() + " km" +" from Samfundet, Trondheim at time "+ new Date(t.getTime()));
	}
}
