package aprs;


public class Attributes {
	
	String name;
	String type;
	int time;
	int lastTime;
	double lat;
	double lng;
	String symbol;
	String srccall;
	String dstcall;
	String phg;
	String comment;
	String path;
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getType() {
		return type;
	}
	
	public void setType(String type) {
		this.type = type;
	}
	
	public long getTime() {
		return time*1000L;
	}
	
	public void setTime(int time) {
		this.time = time;
	}
	
	public long getLastTime() {
		return lastTime;
	}
	
	public void setLastTime(int lastTime) {
		this.lastTime = lastTime;
	}
	
	public double getLat() {
		return lat;
	}
	
	public void setLat(double lat) {
		this.lat = lat;
	}
	
	public double getLon() {
		return lng;
	}
	
	public void setLon(double lon) {
		this.lng = lon;
	}
	
	public String getSymbol() {
		return symbol;
	}
	
	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}
	
	public String getSrccall() {
		return srccall;
	}
	
	public void setSrccall(String srccall) {
		this.srccall = srccall;
	}
	
	public String getDstcall() {
		return dstcall;
	}
	
	public void setDstcall(String dstcall) {
		this.dstcall = dstcall;
	}
	
	public String getPhg() {
		return phg;
	}
	
	public void setPhg(String phg) {
		this.phg = phg;
	}
	
	public String getComment() {
		return comment;
	}
	
	public void setComment(String comment) {
		this.comment = comment;
	}
	
	public String getPath() {
		return path;
	}
	
	public void setPath(String path) {
		this.path = path;
	}
}
