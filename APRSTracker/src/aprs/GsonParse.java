package aprs;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.sql.Date;
import java.util.Iterator;
import java.util.Scanner;

import com.google.gson.Gson;

public class GsonParse {


	//	http://api.aprs.fi/api/get?name=OH7RDA&what=loc&apikey=APIKEY&format=json

	/**
	 * Get the JSON string
	 * @throws IOException 
	 * 
	 */
	public static String aprsfiRetrieveJSONString(String callsign) throws IOException{
		BufferedReader reader = null;
		URL url = new URL("http://api.aprs.fi/api/get?name="+callsign+"&what=loc&apikey="+getApiKey()+"&format=json");
		reader = new BufferedReader(new InputStreamReader(url.openStream()));
		StringBuffer buffer = new StringBuffer();
		int read;
		char[] chars = new char[1024];
		while ((read = reader.read(chars)) != -1)
			buffer.append(chars, 0, read);
		reader.close();
		return buffer.toString();    
	}

	public static String getApiKey(){
		File file = new File(System.getProperty("user.dir") + "/src/aprs/api.key");
		try {
			Scanner scanner = new Scanner(file);
			String apikey = scanner.next();
			scanner.close();
			return apikey;

		} catch (Exception e) {
			throw new NullPointerException("The api.key file was null");
		}
	}

	public static Aprs gsonToObject(String gsonString){
		//move to the good parts..
//		String s = gsonString.substring(gsonString.indexOf("[")+1, gsonString.indexOf("]"));
		Gson gson = new Gson();
		Aprs aprs = gson.fromJson(gsonString, Aprs.class);
		return aprs;
	}



	public static void main(String[] args) {
		
		
		System.out.println(new Date(1270580127*1000L));
		String tag = null;
		try {
			tag = aprsfiRetrieveJSONString("LA5ZKA");
			System.out.println(tag);

			String s = tag.substring(tag.indexOf("[")+1, tag.indexOf("]"));
			Gson g = new Gson();
			Attributes aprs = g.fromJson(tag, Attributes.class);

			
			
//			for (int i = 0; i < aprs.getEntries().size(); i++) {
//			}
			
		} catch (Exception e) {
			System.err.println(e.toString());
		}
	}
}
